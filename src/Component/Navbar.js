// src/components/Navbar.js
import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <nav>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/calender">Calender</Link></li>
                <li><Link to="/EventPlanner">EventPlanner</Link></li>
               
                
            </ul>
        </nav>
    );
};

export default Navbar;
