
import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import HomeIcon from '@mui/icons-material/Home';
import ContactSupporticon from '@mui/icons-material/ContactSupport';
import ChatIcon from '@mui/icons-material/Chat';
import EventIcon from '@mui/icons-material/Event';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import Contactsicon from '@mui/icons-material/Contacts';


const EventDrawer = ({ open, onClose }) => {
  const menuItems = [
    { text: 'Home', path: '/' },
    { text: 'Calender', path: '/Calender' },
  ];
  const menuItems1 = [
    { text: 'EventPlanner', path: '/EventPlanner' },
    { text: 'Contacts', path: '/contacts' }
  ];
  const menuItems2 = [
    'Inbox', 'Chat'
  ];

  const list = (
    <Box
      sx={{ width: 250 }}
      role="presentation"
      onClick={onClose}
      onKeyDown={onClose}
    >
      <List>
        {menuItems.map((item, index) => (
          <ListItem key={item.text} disablePadding>
            <ListItemButton component={Link} to={item.path}>
              <ListItemIcon>
                {index % 2 === 0 ? <HomeIcon /> : <CalendarTodayIcon /> } 
              
              </ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItemButton>
          </ListItem>
        ))}
        {menuItems1.map((item, index) => (
          <ListItem key={item.text} disablePadding>
            <ListItemButton component={Link} to={item.path}>
              <ListItemIcon>
                {index % 2 === 0 ? <EventIcon /> : <Contactsicon /> } 
              
              </ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Divider />
      
      <List>
        {menuItems2.map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <ChatIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <Drawer class="Drawerevent"
      anchor="left"
      open={open}
      onClose={onClose}
    >
      {list}
    </Drawer>
  );
};

export default EventDrawer;
