
import React from 'react';
import '../App.css';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

const EventAppbar = ({ onMenuClick }) => {
  return (
    <Box sx={{ flexGrow: 1, bgcolor: '#3B3131', boxShadow: 'none', color: '#3B3131' }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            aria-label="menu"
            color="inherit"
            sx={{ mr: 2, color: '#ffffff' }}
            onClick={onMenuClick}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            
            sx={{ flexGrow: 1, boxShadow: 'none', color: '#ffffff', cursor: 'pointer' }}
            onClick={onMenuClick}
          > Dashboard
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default EventAppbar;
