import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom';
import { Stepper, Step, StepLabel, Button, Typography, TextField, FormControlLabel, Checkbox } from '@mui/material';
import '../App.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const steps = [
  'Event Name',
  'Choose Date',
  'Choose Type of Event',
  'Choose Location',
  'Choose Start Time',
  'Choose Approximate Duration',
  'Choose Number of Guests',
  'Choose Guests from Guest List',
  'Choose Dress Code',
  'Choose Menu',
  'Description'
];


function getStepContent(step, formData, handleChange) {
  switch (step) {
    case 0:
      return (
          <TextField class='Event_Text1'
            label="Event Name"
            value={formData.eventName}
            onChange={handleChange('eventName')}
            variant="outlined"
            fullWidth
          />
        );
    case 1:
      return (
        <DatePicker class='Event_Text2'
          selected={formData.eventDate}
          onChange={(date) => handleChange('eventDate', date)({target: {value: date}})}
          dateFormat="MMMM d, yyyy"
          placeholderText="Choose Date"
          className="date-picker"
        />
      );
    case 2:
      return (
        <TextField class='Event_Text'
          select
          label="Choose Type of Event"
          value={formData.eventType}
          onChange={handleChange('eventType')}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
          fullWidth
        >
          <option value=""></option>
          <option value="Birthday">Birthday</option>
          <option value="Thanksgiving">Thanksgiving</option>
          <option value="Wedding">Wedding</option>
          <option value="Rice Ceremony">Rice Ceremony</option>
          <option value="Graduation">Graduation</option>
          <option value="Christmas">Christmas</option>
          <option value="Diwali">Diwali</option>
          <option value="Durhgapuja">Durhgapuja</option>
          <option value="Dinner">Dinner</option>
        </TextField>
      );
    case 3:
      return 'Choose Location';
    case 4:
      return (
        <DatePicker class='Event_Text2'
          selected={formData.eventstartTime}
          onChange={(time) => handleChange('eventstartTime', time)({ target: { value: time } })}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={15}
          timeCaption="Start Time"
          dateFormat="h:mm aa"
          placeholderText="Choose Start Time"
          className="time-picker"
        />
      );
    case 5:
      return (
        <TextField class='Event_Text2'
          select
          label="Choose Approximate Duration"
          value={formData.eventApproximateDuration}
          onChange={handleChange('eventApproximateDuration')}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
          fullWidth
        >
          <option value=""></option>
          <option value="30 minutes">30 minutes</option>
          <option value="1 hour">1 hour</option>
          <option value="2 hours">2 hours</option>
          <option value="3 hours">3 hours</option>
          <option value="4 hour">4hour</option>
          <option value="5 hours">5 hours</option>
          <option value="6 hours">6 hours</option>
        </TextField>
      );
    case 6:
      return (
        <TextField class='Event_Text'
          select
          label="Choose Number Of Guests"
          value={formData.eventNumberOfGuests}
          onChange={handleChange('eventNumberOfGuests')}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
          fullWidth
        >
          <option value=""></option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="7">9</option>
          <option value="8">10</option>
        </TextField>
      );
    case 7:
      return (
        <div>
        {['Alan', 'Brian', 'Chan', 'Dipti', 'Ila', 'Paul', 'Pousali','Rini', 'Dave','Lynne',].map((guest) => (
          <FormControlLabel
            key={guest}
            control={
              <Checkbox 
                checked={formData.guests.includes(guest)}
                onChange={handleChange('guests', guest)}
              />
            }
            label={guest}
          />
        ))}
      </div>
         
      );
      
    case 8:
      return (
        <TextField class='Event_Text1'
          select
          label="Choose Type of DressCode"
          value={formData.eventDressCode}
          onChange={handleChange('eventDressCode')}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
          fullWidth
        >
          <option value=""></option>
          <option value="I">Indian</option>
          <option value="P">Party</option>
          <option value="T">Traditional</option>
          <option value="F">Fancy</option>
        </TextField>
      );
    case 9:
      return (
        <div>
          {['Apple pie', 'Salad', 'Kabab', 'Pulao', 'Biriyani', 'Sushi', 'Luchi-Aloodum', 'Ilish Mach'].map((eventMenu) => (
            <FormControlLabel
              key={eventMenu}
              control={
                <Checkbox
                  checked={formData.eventMenu.includes(eventMenu)}
                  onChange={handleChange('eventMenu', eventMenu)}
                />
              }
              label={eventMenu}
            />
          ))}
        </div>
      );
    case 10:
      return (
        <TextField class='Event_Text2'
          label="Drescription"
          value={formData.eventDrescription}
          onChange={handleChange('eventDrescription')}
          variant="outlined"
          fullWidth
        />
      );
    default:
      return 'Unknown step';
  }
}

const EventStepper = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [formData, setFormData] = useState({
    eventName: '',
    eventDate: null,
    eventType: '',
    eventDressCode: '',
    eventDrescription: '',
    eventApproximateDuration: '',
    eventNumberOfGuests: '',
    guests: [],
    eventMenu: [],
    eventstartTime: null,
    // eventGuests:'',
   
  });

  const navigate = useNavigate();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleChange = (field, value) => (event) => {
    if (field === 'guests' ) {
      const newGuests = formData.guests.includes(value)
        ? formData.guests.filter((guest) => guest !== value)
        : [...formData.guests, value];
      setFormData({
        ...formData,
        guests: newGuests,
      });
    } 
    else if(field === 'eventMenu' ) {
      const newMenu = formData.eventMenu.includes(value)
        ? formData.eventMenu.filter((eventMenu) => eventMenu !== value)
        : [...formData.eventMenu, value];
      setFormData({
        ...formData,
        eventMenu: newMenu,
      });}
    else if (field === 'eventDate' || field === 'eventstartTime') {
      setFormData({
        ...formData,
        [field]: value,
      });
    } else {
      setFormData({
        ...formData,
        [field]: event.target.value,
      });
    }
  };
  


  const handleFinish = () => {
    navigate('/review', { state: formData });
  };

  return (
    <div>
      <Stepper activeStep={activeStep}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Typography>{getStepContent(activeStep, formData, handleChange)}</Typography>
            <div>
              <Button disabled={activeStep === 0} onClick={handleBack}>
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={activeStep === steps.length - 1 ? handleFinish : handleNext}
              >
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default EventStepper;