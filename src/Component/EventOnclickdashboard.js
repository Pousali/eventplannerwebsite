
import React, { useState } from 'react';
import EventAppbar from './EventAppbar';
import EventDrawer from './EventDrawer';

const EventOnclickdashboard = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const handleDrawerOpen = () => {
    setDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  return (
    <div>
      <EventAppbar onMenuClick={handleDrawerOpen} />
      <EventDrawer open={drawerOpen} onClose={handleDrawerClose} />
    </div>
  );
};

export default EventOnclickdashboard;
