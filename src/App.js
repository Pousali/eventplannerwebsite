import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import './App.css';
import Calender from './pages/Calender';
import Navbar from './Component/Navbar';
import EventPlanner from './pages/EventPlanner';
import Contact from './pages/Contact';
import EventOnclickdashboard from './Component/EventOnclickdashboard';
import EventPlannerLogIn from './pages/EventPlannerLogIn';
import Review from './pages/Review';

// import Contact from './pages/EventStepper';

function App() {
  return (
 <Router>
   <EventOnclickdashboard />
   {/* <EventPlannerLogIn /> */}
      {/* <Navbar /> */}
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Calender" element={<Calender />} />
        <Route path="/EventPlanner" element={<EventPlanner />} />
        <Route path="/contacts" element={<Contact />} />
        <Route path="/review" element={<Review />} />
        {/* <Route path="/EventStepper" element={<EventStepper />} /> */}
      
      </Routes>
    </Router>
  );
}

export default App;
