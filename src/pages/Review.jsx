// src/pages/Home.js
import React from 'react';
import '../App.css';
import { useLocation } from 'react-router-dom';
import { Typography } from '@mui/material';



const Review = () => {

  const location = useLocation();
  const { state } = location;
    
    return <div className="review">
         <section class='rvd'  >
          <h2 class="Sucessful">Sucessful!</h2>
          <div >
         <Typography variant="h4">Event Summary</Typography>
         <div class='rvd'>
         <Typography variant="body1">Event Name: {state.eventName}</Typography>
         <Typography variant="body1" >
            Event Date: {state?.eventDate ? new Date(state.eventDate).toLocaleDateString() : 'No date selected'}
          </Typography>
          <Typography variant="body1">Event Type: {state.eventType}</Typography>
          <Typography variant="body1">
            Event Start Time: {state?.eventstartTime ? new Date(state.eventstartTime).toLocaleDateString() : 'No date selected'}
          </Typography>
          <Typography variant="body1">Event DressCode: {state.eventDressCode}</Typography>
          <Typography variant="body1">Event Drescription: {state.eventDrescription}</Typography>
          <Typography variant="body1">Event Approximate Duration: {state.eventApproximateDuration}</Typography>
          <Typography variant="body1">Event NumberOfGuests: {state.eventNumberOfGuests}</Typography>
          <Typography variant="body1">Guests: {state.guests.join(', ')}</Typography>
          <Typography variant="body1">Event Menu: {state.eventMenu.join(', ')}</Typography>
          
          
          </div>

    </div>
        </section>
        </div>;

   
  
};

export default Review;
