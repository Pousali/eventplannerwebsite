
import React from 'react';
import '../App.css';
// import EventDrawer from './EventDrawer';


const Home = () => {
    
    return <div className="home-background">
         <section>
          <h2 class="welcome">Welcome To Event Planner Website</h2>
          <p class="content_txt">
          A place where we can plan events, view the calendar, and access contact details.
          </p>
        </section>
        </div>;

   
  
};

export default Home;
