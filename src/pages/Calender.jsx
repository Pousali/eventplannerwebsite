// src/pages/Home.js
import React, { useState } from 'react'; 
import '../App.css';
import Calendar from 'react-calendar';


const About = () => { 
    const [date, setDate] = useState(new Date());

    const Changefunctio = (newDate) => {
        setDate(newDate);
    }

    return(
    <div className="Calender-background">
        Welcome To Calender 
        <Calendar
          Changefunctio={Changefunctio}
          value={date}
            />
        </div>);
};

export default About;
